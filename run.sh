#!/bin/bash

rm -rf out err time
mkdir -p out err time

for i in {1..100}
do
	( time curl --retry 999 --retry-max-time 4000 --max-time 4000 --connect-timeout 4000 -s -i -H "Accept: application/json" -X POST --data @3alamoon.json "https://hafss-server-hihcbmcp3a-uc.a.run.app/find_mistakes" > out/$i 2> err/$i & ) 2> time/$i
done