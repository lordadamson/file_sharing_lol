#!/usr/bin/fish

for i in time/*
	set time $time (cat $i | grep real | cut -d'	' -f 2)
end

for i in $time
	set minutes $minutes (echo $i | cut -d'm' -f 1)
	set seconds $seconds (echo $i | cut -d'm' -f 2 | sed 's| ||g' | sed 's|s||g')
end

for i in (seq 1 (count $minutes))
	set total_time $total_time (math "$minutes[$i] * 60 + $seconds[$i]")	
end

set mean 0

for i in $total_time
	set mean (math "$mean + $i")
end

set len (count $total_time)
set mean (math "$mean / $len")

echo mean: $mean

set max 0

for i in $total_time
	if [ (python -c "print($max < $i)") = "True" ]
		set max $i
	end
end

echo max: $max

set min 999999

for i in $total_time
	if [ (python -c "print($min > $i)") = "True" ]
		set min $i
	end
end

echo min: $min